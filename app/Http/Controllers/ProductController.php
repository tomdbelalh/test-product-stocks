<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductStock;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = [];

        $type = $request['type'] ?? 1;

        $productList = Product::where('is_active', 1)->get(['id', 'name', 'is_active', 'created_by']);

        foreach($productList as $p) {
            $qualified = 0;
            $totalCount = $p->totalCount();

            if($type == 1 || is_null($type)) {
                if($totalCount > 0) $qualified = 1;
            }
            else if ($type == 0) {
                if($totalCount == 0) $qualified = 1;
            }
            else {
                $qualified = 1;
            }

            if($qualified) {
                $products[] = [
                    'id' => $p->id,
                    'name' => $p->name,
                    'is_active' => $p->is_active,
                    'created_by' => $p->user->name,
                    'totalProduct' => $totalCount,
                    'sumPrice' => $p->sumPrice()
                ];
            }
            
        }

        return response()->json($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('add-product', compact(['product']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'count' => 'numeric|min:1',
            'unit_price' => 'numeric|min:.5',
        ]);

        $productStock = new ProductStock;

        $productStock->count = $request->input('count');
        $productStock->unit_price = $request->input('unit_price') * 100;
        $productStock->is_active = $request->input('is_active');
        
        $newStock = $product->productStocks()->save($productStock);

        if($newStock) {
            session()->flash('type', 'success');
            session()->flash('status', 'Product Added Successfully');

            // return redirect()->route('products.index');
            return redirect()->route('home');
        }

        return back();
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
