<?php

namespace App;

use App\ProductStock;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function productStocks () {
    	return $this->hasMany('App\ProductStock');
    }

    public function user () {
    	return $this->belongsTo('App\User', 'created_by');
    }

    public function totalCount()
    {
    	return ProductStock::where('is_active', 1)->where('product_id', $this->id)->sum('count');
    }

    public function sumPrice()
    {
    	return ProductStock::where('is_active', 1)->where('product_id', $this->id)->sum('unit_price') / 100;
    }
    
    
}
