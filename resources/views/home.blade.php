@extends('layouts.app')

@section('content')
<div class="container" id="productList">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h3>Product List (<a @click="loadProducts(1)">Active</a> | <a @click="loadProducts(2)">All</a> | <a @click="loadProducts(0)">Null</a>)</h3>
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Name</th>
                          <th scope="col">Count</th>
                          <th scope="col" class="text-right">Sum Price</th>
                          <th scope="col">Status</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody v-if="products.length">
                        <tr v-for="product in products" :key="product.id">
                          <th scope="row">@{{ product.id }}</th>
                          <td>@{{ product.name }}</td>
                          <td>@{{ product.totalProduct }}</td>
                          <td class="text-right">$ @{{ product.sumPrice }}</td>
                          <td>@{{ statuses[product.is_active] }}</td>
                          <td>
                            <a :href="'/products/' + product.id + '/edit'" class="btn btn-success btn-sm">Add</a>
                          </td>
                        </tr>
                      </tbody>
                      <tbody v-else>
                        <tr>
                          <td colspan="6">No Record Found !!</td>
                        </tr>
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footerScript')
    <script src="{{ asset('jquery.min.js') }}"></script>
    <script src="{{ asset('axios.min.js') }}"></script>
    <script src="{{ asset('vue.js') }}"></script>
    <script>
        var vm = new Vue({
            el: '#productList',
            data: {
                products: [],
                statuses: [
                  'Inactive',
                  'Active',
                ],
            },
            methods: {
                loadProducts: function(type) {
                    var app = this;
                    app.products = [];

                    axios.get('{{ route("products.index") }}?type=' + type)
                      .then(function (response) {
                        // handle success
                        /* 
                        response.data.map(function(product) {
                           console.log(product)
                            app.products.push(product);
                        }) 
                        */

                        app.products = response.data;

                        // console.log(app.products.length)
                      })
                      .catch(function (error) {
                        // handle error
                        // console.log(error);
                      });
                }
            },
            mounted: function() {
                var app = this;
                app.loadProducts('');

                $('#myModal').on('shown.bs.modal', function () {
                  $('#myInput').trigger('focus')
                })
            }
        })
    </script>
@endsection
