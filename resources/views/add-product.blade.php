@extends('layouts.app')

@section('content')
<div class="container" id="productList">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add Product</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                    <form action="{{ route('products.update', ['product' => $product->id]) }}" method="post">
                      @csrf
                      @method('PATCH')
                      
                      <div class="form-group">
                        <label for="email">Product Name:</label>
                        <strong>{{ $product->name }}</strong>
                      </div>

                      <div class="form-group">
                        <label for="count">Count/ Quantity:</label>
                        <input type="text" class="form-control" id="count" name="count">
                      </div>

                      <div class="form-group">
                        <label for="unit_price">Unit Price:</label>
                        <input type="text" class="form-control" id="unit_price" name="unit_price">
                      </div>

                      <div class="checkbox">
                        <label><input type="checkbox" name="is_active" value="1" checked> Active</label>
                      </div>

                      <button type="submit" class="btn btn-success">Save</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footerScript')
@endsection
