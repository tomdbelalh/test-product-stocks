<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	[
        		'name' => 'Test User 1',
                'email' => 'tu1@gmail.com',
                'password' => Hash::make('tu1-123456'),
        	],
            [
                'name' => 'Test User 2',
                'email' => 'tu2@gmail.com',
                'password' => Hash::make('tu2-123456'),
            ],
            [
                'name' => 'Test User 3',
                'email' => 'tu3@gmail.com',
                'password' => Hash::make('tu3-123456'),
            ],
            [
                'name' => 'Test User 4',
                'email' => 'tu4@gmail.com',
                'password' => Hash::make('tu4-123456'),
            ],
            [
                'name' => 'Test User 5',
                'email' => 'tu5@gmail.com',
                'password' => Hash::make('tu5-123456'),
            ],
        ]);
    }
}
