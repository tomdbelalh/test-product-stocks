<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
        	[
        		'name' => 'Computer',
        		'created_by' => 1,
        	],
        	[
        		'name' => 'Laptop',
        		'created_by' => 1,
        	],
        	[
        		'name' => 'Keyboard',
        		'created_by' => 1,
        	],
        	[
        		'name' => 'Mouse',
        		'created_by' => 1,
        	],
        	[
        		'name' => 'RAM',
        		'created_by' => 1,
        	],
        	[
        		'name' => 'Pen',
        		'created_by' => 2,
        	],
        	[
        		'name' => 'Pencil',
        		'created_by' => 2,
        	],
        	[
        		'name' => 'Eraser',
        		'created_by' => 3,
        	],
        ]);
    }
}
